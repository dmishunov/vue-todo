import Vue from 'vue'
import Router from 'vue-router'

import ListTodoList from './views/todoList/ListTodoList.vue'
import NewTodoList from './views/todoList/NewTodoList.vue'
import EditTodoList from './views/todoList/EditTodoList.vue'

import ListTodoItem from './views/todoItem/ListTodoItem.vue'
import NewTodoItem from './views/todoItem/NewTodoItem.vue'
import EditTodoItem from './views/todoItem/EditTodoItem.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'notHome',
      redirect: '/list' },
    {
      path: '/list',
      name: 'listTodoList',
      component: ListTodoList,
    },
    {
      path: '/list/new',
      name: 'newTodoList',
      component: NewTodoList
    },
    {
      path: '/list/:id/edit',
      name: 'editTodoList',
      component: EditTodoList,
      props: true
    },
    {
      path: '/list/:id/todo',
      name: 'listTodoItem',
      component: ListTodoItem,
      props: true
    },
    {
      path: '/list/:id/todo/new',
      name: 'newTodoItem',
      component: NewTodoItem,
      props: true
    },
    {
      path: '/list/:id/todo/:todoId/edit',
      name: 'editTodoItem',
      component: EditTodoItem,
      props: true
    }
  ]
})